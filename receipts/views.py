from django.shortcuts import render, redirect
from django.views.generic import ListView, RedirectView, CreateView
from receipts.models import ExpenseCategory, Receipt
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts_list_view.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptRedirectView(RedirectView):
    url = "/receipts/"
    pattern_name = "home"


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    tempate_name = "create.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect()
